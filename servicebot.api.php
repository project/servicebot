<?php

/**
 * @file
 * API documentation for Servicebot.
 */

/**
 * React to the stripe event when a new subscription is created.
 *
 * Occurs whenever a customer is signed up for a new plan.
 *
 * @param array $stripe_subscription_object
 *   Subscriptions allow you to charge a customer on a recurring basis.
 *    - id:                                 string Unique identifier for the object.
 *    - cancel_at_period_end:               boolean If the subscription has been canceled with the at_period_end flag set to true, cancel_at_period_end on the subscription will be true. You can use this attribute to determine whether a subscription that has a status of active is scheduled to be canceled at the end of the current period.
 *    - current_period_end:                 timestamp End of the current period that the subscription has been invoiced for. At the end of this period, a new invoice will be created.
 *    - current_period_start:                timestamp Start of the current period that the subscription has been invoiced for.
 *    - customer:                           string ID of the customer who owns the subscription.
 *    - default_payment_method              string ID of the default payment method for the subscription. It must belong to the customer associated with the subscription. If not set, invoices will use the default payment method in the customer’s invoice settings.
 *    - items:                              array List of subscription items, each with an attached plan.
 *    - latest_invoice:                     string The most recent invoice this subscription has generated.
 *    - metadata:                           array Set of key-value pairs that you can attach to an object. This can be useful for storing additional information about the object in a structured format.
 *    - pending_setup_intent:               string You can use this SetupIntent to collect user authentication when creating a subscription without immediate payment or updating a subscription’s payment method, allowing you to optimize for off-session payments. Learn more in the SCA Migration Guide.
 *    - pending_update:                     array If specified, pending updates that will be applied to the subscription once the latest_invoice has been paid.
 *    - status:                             string Possible values are incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid.
 *    - object:                             string String representing the object’s type. Objects of the same type share the same value.
 *    - application_fee_percent:            float A non-negative decimal between 0 and 100, with at most two decimal places. This represents the percentage of the subscription invoice subtotal that will be transferred to the application owner’s Stripe account.
 *    - billing_cycle_anchor:               timestamp Determines the date of the first full invoice, and, for plans with month or year intervals, the day of the month for subsequent invoices.
 *    - billing_threshold:                  array Define thresholds at which an invoice will be sent, and the subscription advanced to a new billing period
 *    - cancel_at:                          timestamp A date in the future at which the subscription will automatically get canceled
 *    - canceled_at:                        timestamp If the subscription has been canceled, the date of that cancellation. If the subscription was canceled with cancel_at_period_end, canceled_at will still reflect the date of the initial cancellation request, not the end of the subscription period when the subscription is automatically moved to a canceled state.
 *    - collection_method:                  string Either charge_automatically, or send_invoice. When charging automatically, Stripe will attempt to pay this subscription at the end of the cycle using the default source attached to the customer. When sending an invoice, Stripe will email your customer an invoice with payment instructions.
 *    - created:                            timestamp Time at which the object was created. Measured in seconds since the Unix epoch.
 *    - days_until_due:                     int Number of days a customer has to pay invoices generated by this subscription. This value will be null for subscriptions where collection_method=charge_automatically
 *    - default_source:                     string ID of the default payment source for the subscription. It must belong to the customer associated with the subscription and be in a chargeable state. If not set, defaults to the customer’s default source.
 *    - default_tax_rates:                  array The tax rates that will apply to any subscription item that does not have tax_rates set. Invoices created will have their default_tax_rates populated from the subscription.
 *    - discount:                           array Describes the current discount applied to this subscription, if there is one. When billing, a discount applied to a subscription overrides a discount applied on a customer-wide basis.
 *    - ended_at:                           timestamp If the subscription has ended, the date the subscription ended.
 *    - livemode:                           boolean Has the value true if the object exists in live mode or the value false if the object exists in test mode.
 *    - next_pending_invoice_item_invoice:  timestamp Specifies the approximate timestamp on which any pending invoice items will be billed according to the schedule provided at pending_invoice_item_interval.
 *    - pending_invoice_item_interval:      array Specifies an interval for how often to bill for any pending invoice items. It is analogous to calling Create an invoice for the given subscription at the specified interval.
 *    - plan:                               string Hash describing the plan the customer is subscribed to. Only set if the subscription contains a single plan.
 *    - quantity:                           int The quantity of the plan to which the customer is subscribed. For example, if your plan is $10/user/month, and your customer has 5 users, you could pass 5 as the quantity to have the customer charged $50 (5 x $10) monthly. Only set if the subscription contains a single plan.
 *    - schedule:                           string The schedule attached to the subscription
 *    - start_date:                         timestamp Date when the subscription was first created. The date might differ from the created date due to backdating.
 *    - trail_end:                          timestamp If the subscription has a trial, the end of that trial.
 *    -trail_start:                         timestamp If the subscription has a trial, the beginning of that trial.
 *
 * @See https://stripe.com/docs/api/subscriptions
 */
function hook_servicebot_customer_subscription_created($stripe_subscription_object) {

}

/**
 * React to the stripe event when an existing subscription is updated.
 *
 * Occurs whenever a subscription changes (e.g., switching from one plan to another, or changing the status from trial to active).
 *
 * @param array $stripe_subscription_object
 *   Subscriptions allow you to charge a customer on a recurring basis.
 *    - id:                                 string Unique identifier for the object.
 *    - cancel_at_period_end:               boolean If the subscription has been canceled with the at_period_end flag set to true, cancel_at_period_end on the subscription will be true. You can use this attribute to determine whether a subscription that has a status of active is scheduled to be canceled at the end of the current period.
 *    - current_period_end:                 timestamp End of the current period that the subscription has been invoiced for. At the end of this period, a new invoice will be created.
 *    - current_period_start:                timestamp Start of the current period that the subscription has been invoiced for.
 *    - customer:                           string ID of the customer who owns the subscription.
 *    - default_payment_method              string ID of the default payment method for the subscription. It must belong to the customer associated with the subscription. If not set, invoices will use the default payment method in the customer’s invoice settings.
 *    - items:                              array List of subscription items, each with an attached plan.
 *    - latest_invoice:                     string The most recent invoice this subscription has generated.
 *    - metadata:                           array Set of key-value pairs that you can attach to an object. This can be useful for storing additional information about the object in a structured format.
 *    - pending_setup_intent:               string You can use this SetupIntent to collect user authentication when creating a subscription without immediate payment or updating a subscription’s payment method, allowing you to optimize for off-session payments. Learn more in the SCA Migration Guide.
 *    - pending_update:                     array If specified, pending updates that will be applied to the subscription once the latest_invoice has been paid.
 *    - status:                             string Possible values are incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid.
 *    - object:                             string String representing the object’s type. Objects of the same type share the same value.
 *    - application_fee_percent:            float A non-negative decimal between 0 and 100, with at most two decimal places. This represents the percentage of the subscription invoice subtotal that will be transferred to the application owner’s Stripe account.
 *    - billing_cycle_anchor:               timestamp Determines the date of the first full invoice, and, for plans with month or year intervals, the day of the month for subsequent invoices.
 *    - billing_threshold:                  array Define thresholds at which an invoice will be sent, and the subscription advanced to a new billing period
 *    - cancel_at:                          timestamp A date in the future at which the subscription will automatically get canceled
 *    - canceled_at:                        timestamp If the subscription has been canceled, the date of that cancellation. If the subscription was canceled with cancel_at_period_end, canceled_at will still reflect the date of the initial cancellation request, not the end of the subscription period when the subscription is automatically moved to a canceled state.
 *    - collection_method:                  string Either charge_automatically, or send_invoice. When charging automatically, Stripe will attempt to pay this subscription at the end of the cycle using the default source attached to the customer. When sending an invoice, Stripe will email your customer an invoice with payment instructions.
 *    - created:                            timestamp Time at which the object was created. Measured in seconds since the Unix epoch.
 *    - days_until_due:                     int Number of days a customer has to pay invoices generated by this subscription. This value will be null for subscriptions where collection_method=charge_automatically
 *    - default_source:                     string ID of the default payment source for the subscription. It must belong to the customer associated with the subscription and be in a chargeable state. If not set, defaults to the customer’s default source.
 *    - default_tax_rates:                  array The tax rates that will apply to any subscription item that does not have tax_rates set. Invoices created will have their default_tax_rates populated from the subscription.
 *    - discount:                           array Describes the current discount applied to this subscription, if there is one. When billing, a discount applied to a subscription overrides a discount applied on a customer-wide basis.
 *    - ended_at:                           timestamp If the subscription has ended, the date the subscription ended.
 *    - livemode:                           boolean Has the value true if the object exists in live mode or the value false if the object exists in test mode.
 *    - next_pending_invoice_item_invoice:  timestamp Specifies the approximate timestamp on which any pending invoice items will be billed according to the schedule provided at pending_invoice_item_interval.
 *    - pending_invoice_item_interval:      array Specifies an interval for how often to bill for any pending invoice items. It is analogous to calling Create an invoice for the given subscription at the specified interval.
 *    - plan:                               string Hash describing the plan the customer is subscribed to. Only set if the subscription contains a single plan.
 *    - quantity:                           int The quantity of the plan to which the customer is subscribed. For example, if your plan is $10/user/month, and your customer has 5 users, you could pass 5 as the quantity to have the customer charged $50 (5 x $10) monthly. Only set if the subscription contains a single plan.
 *    - schedule:                           string The schedule attached to the subscription
 *    - start_date:                         timestamp Date when the subscription was first created. The date might differ from the created date due to backdating.
 *    - trail_end:                          timestamp If the subscription has a trial, the end of that trial.
 *    -trail_start:                         timestamp If the subscription has a trial, the beginning of that trial.
 *
 * @See https://stripe.com/docs/api/subscriptions
 */
function hook_servicebot_customer_subscription_updated($stripe_subscription_object) {

}

/**
 * React to the stripe event when an existing subscription is updated.
 *
 * Occurs whenever a customer's subscription ends.
 *
 * @param array $stripe_subscription_object
 *   Subscriptions allow you to charge a customer on a recurring basis.
 *    - id:                                 string Unique identifier for the object.
 *    - cancel_at_period_end:               boolean If the subscription has been canceled with the at_period_end flag set to true, cancel_at_period_end on the subscription will be true. You can use this attribute to determine whether a subscription that has a status of active is scheduled to be canceled at the end of the current period.
 *    - current_period_end:                 timestamp End of the current period that the subscription has been invoiced for. At the end of this period, a new invoice will be created.
 *    - current_period_start:                timestamp Start of the current period that the subscription has been invoiced for.
 *    - customer:                           string ID of the customer who owns the subscription.
 *    - default_payment_method              string ID of the default payment method for the subscription. It must belong to the customer associated with the subscription. If not set, invoices will use the default payment method in the customer’s invoice settings.
 *    - items:                              array List of subscription items, each with an attached plan.
 *    - latest_invoice:                     string The most recent invoice this subscription has generated.
 *    - metadata:                           array Set of key-value pairs that you can attach to an object. This can be useful for storing additional information about the object in a structured format.
 *    - pending_setup_intent:               string You can use this SetupIntent to collect user authentication when creating a subscription without immediate payment or updating a subscription’s payment method, allowing you to optimize for off-session payments. Learn more in the SCA Migration Guide.
 *    - pending_update:                     array If specified, pending updates that will be applied to the subscription once the latest_invoice has been paid.
 *    - status:                             string Possible values are incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid.
 *    - object:                             string String representing the object’s type. Objects of the same type share the same value.
 *    - application_fee_percent:            float A non-negative decimal between 0 and 100, with at most two decimal places. This represents the percentage of the subscription invoice subtotal that will be transferred to the application owner’s Stripe account.
 *    - billing_cycle_anchor:               timestamp Determines the date of the first full invoice, and, for plans with month or year intervals, the day of the month for subsequent invoices.
 *    - billing_threshold:                  array Define thresholds at which an invoice will be sent, and the subscription advanced to a new billing period
 *    - cancel_at:                          timestamp A date in the future at which the subscription will automatically get canceled
 *    - canceled_at:                        timestamp If the subscription has been canceled, the date of that cancellation. If the subscription was canceled with cancel_at_period_end, canceled_at will still reflect the date of the initial cancellation request, not the end of the subscription period when the subscription is automatically moved to a canceled state.
 *    - collection_method:                  string Either charge_automatically, or send_invoice. When charging automatically, Stripe will attempt to pay this subscription at the end of the cycle using the default source attached to the customer. When sending an invoice, Stripe will email your customer an invoice with payment instructions.
 *    - created:                            timestamp Time at which the object was created. Measured in seconds since the Unix epoch.
 *    - days_until_due:                     int Number of days a customer has to pay invoices generated by this subscription. This value will be null for subscriptions where collection_method=charge_automatically
 *    - default_source:                     string ID of the default payment source for the subscription. It must belong to the customer associated with the subscription and be in a chargeable state. If not set, defaults to the customer’s default source.
 *    - default_tax_rates:                  array The tax rates that will apply to any subscription item that does not have tax_rates set. Invoices created will have their default_tax_rates populated from the subscription.
 *    - discount:                           array Describes the current discount applied to this subscription, if there is one. When billing, a discount applied to a subscription overrides a discount applied on a customer-wide basis.
 *    - ended_at:                           timestamp If the subscription has ended, the date the subscription ended.
 *    - livemode:                           boolean Has the value true if the object exists in live mode or the value false if the object exists in test mode.
 *    - next_pending_invoice_item_invoice:  timestamp Specifies the approximate timestamp on which any pending invoice items will be billed according to the schedule provided at pending_invoice_item_interval.
 *    - pending_invoice_item_interval:      array Specifies an interval for how often to bill for any pending invoice items. It is analogous to calling Create an invoice for the given subscription at the specified interval.
 *    - plan:                               string Hash describing the plan the customer is subscribed to. Only set if the subscription contains a single plan.
 *    - quantity:                           int The quantity of the plan to which the customer is subscribed. For example, if your plan is $10/user/month, and your customer has 5 users, you could pass 5 as the quantity to have the customer charged $50 (5 x $10) monthly. Only set if the subscription contains a single plan.
 *    - schedule:                           string The schedule attached to the subscription
 *    - start_date:                         timestamp Date when the subscription was first created. The date might differ from the created date due to backdating.
 *    - trail_end:                          timestamp If the subscription has a trial, the end of that trial.
 *    -trail_start:                         timestamp If the subscription has a trial, the beginning of that trial.
 *
 * @See https://stripe.com/docs/api/subscriptions
 */
function hook_servicebot_customer_subscription_deleted($stripe_subscription_object) {

}

/**
 * React to the stripe event when an existing subscription is updated.
 *
 * Occurs whenever a new source is created for a customer.
 *
 * @param array $stripe_subscription_object
 *
 * @see https://stripe.com/docs/api/cards/object
 */
function hook_servicebot_customer_source_created($stripe_subscription_object) {

}

/**
 * React to the stripe event when an existing subscription is updated.
 *
 * Occurs whenever a source's details are changed.
 *
 * @param array $stripe_subscription_object
 *
 * @see https://stripe.com/docs/api/cards/object
 */
function hook_servicebot_customer_source_updated($stripe_subscription_object) {

}

/**
 * React to the stripe event when an existing subscription is updated.
 *
 * Occurs whenever a card or source will expire at the end of the month.
 *
 * @param array $stripe_subscription_object
 *
 * @see https://stripe.com/docs/api/cards/object
 */
function hook_servicebot_customer_source_expiring($stripe_subscription_object) {

}

/**
 * React to the stripe event when an existing subscription is updated.
 *
 * Occurs three days before a subscription's trial period is scheduled to end, or when a trial is ended immediately (using trial_end=now).
 *
 * @param array $stripe_subscription_object
 *   Subscriptions allow you to charge a customer on a recurring basis.
 *    - id:                                 string Unique identifier for the object.
 *    - cancel_at_period_end:               boolean If the subscription has been canceled with the at_period_end flag set to true, cancel_at_period_end on the subscription will be true. You can use this attribute to determine whether a subscription that has a status of active is scheduled to be canceled at the end of the current period.
 *    - current_period_end:                 timestamp End of the current period that the subscription has been invoiced for. At the end of this period, a new invoice will be created.
 *    - current_period_start:                timestamp Start of the current period that the subscription has been invoiced for.
 *    - customer:                           string ID of the customer who owns the subscription.
 *    - default_payment_method              string ID of the default payment method for the subscription. It must belong to the customer associated with the subscription. If not set, invoices will use the default payment method in the customer’s invoice settings.
 *    - items:                              array List of subscription items, each with an attached plan.
 *    - latest_invoice:                     string The most recent invoice this subscription has generated.
 *    - metadata:                           array Set of key-value pairs that you can attach to an object. This can be useful for storing additional information about the object in a structured format.
 *    - pending_setup_intent:               string You can use this SetupIntent to collect user authentication when creating a subscription without immediate payment or updating a subscription’s payment method, allowing you to optimize for off-session payments. Learn more in the SCA Migration Guide.
 *    - pending_update:                     array If specified, pending updates that will be applied to the subscription once the latest_invoice has been paid.
 *    - status:                             string Possible values are incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid.
 *    - object:                             string String representing the object’s type. Objects of the same type share the same value.
 *    - application_fee_percent:            float A non-negative decimal between 0 and 100, with at most two decimal places. This represents the percentage of the subscription invoice subtotal that will be transferred to the application owner’s Stripe account.
 *    - billing_cycle_anchor:               timestamp Determines the date of the first full invoice, and, for plans with month or year intervals, the day of the month for subsequent invoices.
 *    - billing_threshold:                  array Define thresholds at which an invoice will be sent, and the subscription advanced to a new billing period
 *    - cancel_at:                          timestamp A date in the future at which the subscription will automatically get canceled
 *    - canceled_at:                        timestamp If the subscription has been canceled, the date of that cancellation. If the subscription was canceled with cancel_at_period_end, canceled_at will still reflect the date of the initial cancellation request, not the end of the subscription period when the subscription is automatically moved to a canceled state.
 *    - collection_method:                  string Either charge_automatically, or send_invoice. When charging automatically, Stripe will attempt to pay this subscription at the end of the cycle using the default source attached to the customer. When sending an invoice, Stripe will email your customer an invoice with payment instructions.
 *    - created:                            timestamp Time at which the object was created. Measured in seconds since the Unix epoch.
 *    - days_until_due:                     int Number of days a customer has to pay invoices generated by this subscription. This value will be null for subscriptions where collection_method=charge_automatically
 *    - default_source:                     string ID of the default payment source for the subscription. It must belong to the customer associated with the subscription and be in a chargeable state. If not set, defaults to the customer’s default source.
 *    - default_tax_rates:                  array The tax rates that will apply to any subscription item that does not have tax_rates set. Invoices created will have their default_tax_rates populated from the subscription.
 *    - discount:                           array Describes the current discount applied to this subscription, if there is one. When billing, a discount applied to a subscription overrides a discount applied on a customer-wide basis.
 *    - ended_at:                           timestamp If the subscription has ended, the date the subscription ended.
 *    - livemode:                           boolean Has the value true if the object exists in live mode or the value false if the object exists in test mode.
 *    - next_pending_invoice_item_invoice:  timestamp Specifies the approximate timestamp on which any pending invoice items will be billed according to the schedule provided at pending_invoice_item_interval.
 *    - pending_invoice_item_interval:      array Specifies an interval for how often to bill for any pending invoice items. It is analogous to calling Create an invoice for the given subscription at the specified interval.
 *    - plan:                               string Hash describing the plan the customer is subscribed to. Only set if the subscription contains a single plan.
 *    - quantity:                           int The quantity of the plan to which the customer is subscribed. For example, if your plan is $10/user/month, and your customer has 5 users, you could pass 5 as the quantity to have the customer charged $50 (5 x $10) monthly. Only set if the subscription contains a single plan.
 *    - schedule:                           string The schedule attached to the subscription
 *    - start_date:                         timestamp Date when the subscription was first created. The date might differ from the created date due to backdating.
 *    - trail_end:                          timestamp If the subscription has a trial, the end of that trial.
 *    -trail_start:                         timestamp If the subscription has a trial, the beginning of that trial.
 *
 * @See https://stripe.com/docs/api/subscriptions
 */
function hook_servicebot_customer_subscription_trial_will_end($stripe_subscription_object) {

}

/**
 * React to the stripe event when an existing subscription is updated.
 *
 * Occurs whenever an invoice payment attempt fails, due either to a declined payment or to the lack of a stored payment method.
 *
 *
 * @param array $stripe_invoice_object
 *
 * @see https://stripe.com/docs/api/invoices/object
 */
function hook_servicebot_invoice_payment_failed($stripe_invoice_object) {

}
